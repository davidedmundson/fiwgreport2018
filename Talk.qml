/***************************************************************************
 *   Copyright (C) 2017 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import "presentation"
import QtQuick 2.6

Presentation
{
    id: presentation

    width: square ? 1024 : 1280
    height: square ? 768 : 720

    property bool square: false

    property variant fromSlide: null
    property variant toSlide: null

    property int transitionTime: 0

    fontFamily: "Noto Sans"

    showNotes: false

    Image {
        id: transitionLogo

        visible: false

        width: Math.min(parent.width/2, parent.height/2)
        height: width

        opacity: 0.0

        z: 9999

        anchors.centerIn: parent

        smooth: true
        source: "kdeev.svg"
        sourceSize.width: width
        sourceSize.height: height
    }

    Image {
        opacity: (presentation.currentSlide > 1) ? 1.0 : 0.0

        Behavior on opacity {
            NumberAnimation { duration: presentation.transitionTime/2; easing.type: Easing.InQuart }
        }

        width: (slideCounter.font.pixelSize * 3.2)
        height: width/2.022

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: slideCounter.font.pixelSize

        smooth: true
        source: "kdeev.svg"
        sourceSize.width: width
        sourceSize.height: height
    }

    SequentialAnimation {
        id: forwardTransition
        alwaysRunToEnd: true

        PropertyAction { target: toSlide; property: "visible"; value: true }
        PropertyAction { target: transitionLogo; property: "visible"; value: ((presentation.currentSlide == 0) ? true : false) }
        ParallelAnimation {
            NumberAnimation { target: fromSlide; property: "opacity"; from: 1; to: 0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: fromSlide; property: "scale"; from: 1; to: 1.1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: toSlide; property: "opacity"; from: 0; to: 1; duration: presentation.transitionTime; easing.type: Easing.InQuart }
            NumberAnimation { target: toSlide; property: "scale"; from: 0.7; to: 1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: transitionLogo; property: "opacity"; from: 0.4; to: 0.0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: transitionLogo; property: "scale"; from: 0.8; to: 4; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
        }
        PropertyAction { target: transitionLogo; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "scale"; value: 1 }
    }

    SequentialAnimation {
        id: backwardTransition
        alwaysRunToEnd: true

        PropertyAction { target: toSlide; property: "visible"; value: true }
        ParallelAnimation {
            NumberAnimation { target: fromSlide; property: "opacity"; from: 1; to: 0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: fromSlide; property: "scale"; from: 1; to: 0.7; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: toSlide; property: "opacity"; from: 0; to: 1; duration: presentation.transitionTime; easing.type: Easing.InQuart }
            NumberAnimation { target: toSlide; property: "scale"; from: 1.1; to: 1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
        }
        PropertyAction { target: fromSlide; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "scale"; value: 1 }
    }

    function switchSlides(from, to, forward)
    {
        if (forwardTransition.running || backwardTransition.running) {
            return false;
        }

        presentation.fromSlide = from;
        presentation.toSlide = to;

        if (forward) {
            forwardTransition.start();
        } else {
            backwardTransition.start();
        }

        return true;
    }

    SlideCounter { id: slideCounter }

    Slide {
        fontFamily: "Noto Sans"

        Image {
            width: parent.height/1.6
            height: width/2.022

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: -(parent.height/12)

            verticalAlignment: Image.AlignTop

            smooth: true
            source: "kdeev.svg"
            sourceSize.width: width
            sourceSize.height: height
        }

        centeredText: "<h1></h1><h1></h1><h1></h1>\n<h2><b>Financial Working Group</b></h2><h5>— Akademy Report 2021 —</h5>\n<h5>June 2021</h5>"
    }

    Slide {
        title: "Mission & Goals"

        fontScale: 0.70

        content: [
            "Support KDE e.V. Board in Financial topics.",
            "In collaboration with the Treasurer, propose financial decisions to the KDE e.V. Board.",
            "Support Board members in commercial activities.",
            "Support the Treasurer in communication actions related to financial topics.",
            "Evaluate financial reports submitted by the Treasurer.",
            "Help the Community implement the annual Budget Plan.",
        ]
    }

    Slide {
        title: "Members: Elected for two-year terms"

        fontScale: 0.60
        textFormat: Text.RichText

        content: [
            "Daniele E. Domenichelli (ddomenichelli@drdanz.it)<br />Until April 14th, 2021.",
            "David Edmundson (david@davidedmundson.co.uk)<br />Until May 19th, 2021.",
            "Eike Hein (hein@kde.org)<br /><font color=\"green\">Active.</font> Treasurer.",
            "Marta Rybczynska (marta.rybczynska@kde.org)<br /><font color=\"green\">Active.</font> Elected on May 26th, 2021.",
            "Neofytos Kolokotronis (neofytosk@posteo.net)<br />Until May 19th, 2021.",
            "Till Adam (adam@kde.org)<br /><font color=\"green\">Active.</font> Elected on May 26th, 2021."
        ]
    }

    Slide {
        title: "2020 Financial Overview"

        fontScale: 0.7

        content: [
          "Admist pandemic-fueled uncertainty, we revised mid-year for a fiscally conservative approach.",
          "As Akademy and other events converted to online-only formats, travel expenses reduced greatly.",
          " Some of this budget was re-allocated to set up reusable online event infrastructure.",
          " We hired an Event Coordinator to help with organization and transitioning the format.",
          "Our major income sources remained stable or improved in 2020.",
          " We received a further large one-time donation by the Handshake Foundation.",
          " Other one-time donations increased by 40%.",
          "Income from student mentorship programs decreased significantly.",
          " Interest in GSoC/Code-In seems to be trending down.",
          "Overall, our financial assets grew in 2020."
        ]
    }

    Slide {
        title: "Income 2015-2020"
        TotalIn {
            anchors.fill: parent
        }
    }

    Slide {
        title: "Expenses 2015-2020"
        TotalOut {
            anchors.fill: parent
        }
    }

    Slide {
        title: "2020 in Detail"
        Total2020Split {
            anchors.fill: parent
        }
    }

    Slide {
        title: "2021 Budget Plan"

        fontScale: 0.68

        content: [
          "Done in collaboration.",
          " Drafted by the Treasurer with input from the KDE e.V. board & other stakeholders.",
          " Reviewed and improved with feedback by the Financial Working Group.",
          "We expect our income to remain stable this year.",
          "We expect travel expenditure to remain low, while allowing for a possible return to travel in Q4/21.",
          " Our main events will remain virtual/online this year.",
          "We are greatly increasing the volume of contracted work throughout 2021.",
          " 3x software devs (20h), 1x tech writer (20h), 1x docs tooling (10h/short duration) from the core KDE eV budget.",
          " 1x software dev, 1x Krita doc writer (low volume) funded by the 2018 Calligra-earmarked Handshake donation.",
          " 2x software devs funded by \"Blauer-Engel-FOSS\" energy efficiency project from the Environmental Agency of Germany.",
          "Our aim is to stimulate growth in the KDE ecosystem to sustain KDE e.V.'s growth."
        ]
    }

    Slide {
        title: "2021 Mid-Year Review & Future Outlook"

        fontScale: 0.64

        content: [
            "We have gained two new Patrons: Pine64 and Slimbook, both also Akademy 2021 sponsors. Thank you!",
            "We have also received a large donation of almost 25.000 EUR by Pine64, in relation to the PinePhone KDE Community Edition. Thank you!",
            "General donation income remains stable, and trends up.",
            "We have received all outstanding payments related to 2020 activities.",
            " For example, Linux App Summit 2020 sponsorship proceeds.",
            "Travel expenditure has been 0.",
            "Hiring for the new contracted positions has progressed as planned.",
            "2022: We will see the combined effects of staff growth and the return to travel.",
            " We expect to significantly outspend our income in this and especially next year. This is intentional to use our currently high balance.",
            " If we wish to sustain this level of contractors after that we will need to grow our income to match within only a few years."
        ]
    }

    Slide {
        fontScale: 2
        centeredText: "<b>Q & A</b>"
    }
}
