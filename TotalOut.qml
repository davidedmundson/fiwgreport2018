import QtQuick 2.0
import QtCharts 2.0

ChartView {
    title: "Expenses"
    titleFont.bold: true
    titleFont.pointSize: 15
    legend.alignment: Qt.AlignBottom
    legend.font.pointSize: 14
    antialiasing: true
    width: 500
    height: 500

    StackedBarSeries {
        id: mySeries
        axisY: ValueAxis {
            min: 0
            max: 600000
            labelFormat: "%d (EUR)"
        }
        axisX: BarCategoryAxis { categories: ["2015", "2016", "2017", "2018", "2019", "2020"] }
        BarSet { label: "Expenses (minus Akademy)"; values: [0, 85992, 76172, 141569, 188182.75, 172402.65] }
        BarSet { label: "Akademy"; values: [0, 0, 71275, 68459, 70668.37, 6847.40] }
        BarSet { color: "#41cd52"; label: "QtCon (replaced Akademy)"; values: [0, 253115, 0, 0, 0] }
        BarSet { color: "#129eaa"; label: "Expenses"; values: [79800, 0, 0, 0, 0]}
    }
}
