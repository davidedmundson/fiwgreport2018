import QtQuick 2.0
import QtCharts 2.0

ChartView {
    title: "Income"
    titleFont.bold: true
    titleFont.pointSize: 15
    legend.alignment: Qt.AlignBottom
    legend.font.pointSize: 14
    antialiasing: true
    width: 500
    height: 500

    StackedBarSeries {
        id: mySeries
        axisX: BarCategoryAxis { categories: ["2015", "2016", "2017", "2018", "2019", "2020*"] }
        axisY: ValueAxis {
            min: 0
            max: 600000
            labelFormat: "%d (EUR)"
        }
        BarSet { label: "Income (minus Akademy)"; values: [78870, 136885, 121998, 132785, 132238.81, 115312.31] }
        BarSet { label: "Akademy"; values: [46940.39, 0, 28222, 36844, 51644.14, 24430.00] }
        BarSet { color: "#41cd52"; label: "QtCon (replaced Akademy)"; values: [0, 214390, 0, 0, 0, 0] }
        BarSet { color: "#289c45"; label: "Large one-time donations"; values: [0, 0, 0, 421987, 0, 79081.56] }
    }

    Text {
        anchors.left: parent.left
        anchors.leftMargin: parent.width * 0.5963
        anchors.top: parent.top
        anchors.topMargin: parent.height * 0.31
        font.family: "Noto Color Emoji"
        font.pointSize: 29
        horizontalAlignment: Text.AlignHCenter
        text: "🍍\n🤝"
    }
    
    Text {
        anchors.left: parent.left
        anchors.leftMargin: parent.width * 0.887
        anchors.top: parent.top
        anchors.topMargin: parent.height * 0.571
        font.family: "Noto Color Emoji"
        font.pointSize: 29
        horizontalAlignment: Text.AlignHCenter
        text: "🤝"
    }
    
    Text {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -(parent.height * 0.09)
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.85
        font.family: "Noto Sans"
        font.pointSize: 11
        wrapMode: Text.Wrap
        text: "* = An unusually high amount of 23.500 EUR of corporate membership fees invoiced in 2020 was received at a delay in 2021. This income will be shown for the year 2021 in the next report. Taken into account, income excluding event sponsoring and large one-time donations (blue) increased slightly in 2020. Also received in 2021 was sponsorship income for Linux App Summit (LAS) 2020."
    }
}
